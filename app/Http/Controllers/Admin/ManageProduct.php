<?php

namespace App\Http\Controllers\Admin;

use App\Model\Product\CreateProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageProduct extends Controller
{

     public function __construct()

        {

            $this->middleware('auth:admin');

        }



        public function CreateProduct(){

         $allprod = CreateProduct::all();

        return view('admin.pages.product.create')->with([
            'prods' => $allprod
        ]);

    }

    public function CreateProductSave(Request $request){
         $a  = new CreateProduct();
         $a->name = $request->name;
         $a->description = $request->description;
         $a->pricing = $request->pricing;
         $a->currency = $request->currency;
         $a->save();

         return back();
    }
}
