<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function CreateCustomer(Request $request){
        $a = new User();
        $a->fname = $request->fname;
        $a->lname = $request->lname;
        $a->email = $request->email;
        $a->phone = $request->phone;
        $a->password = bcrypt($request->password);
        $a->country = $request->country;

        $a->save();

        return back();
    }

    public function ListCustomer(){




        return view('admin.pages.allcustomer');
    }

    public function GetCustonmerAPI(){
        $customers = User::select([
            'id',
            'fname',
            'lname',
            'phone',
            'email',
            'country',

        ])->orderBy('created_at', 'DESC');

        return DataTables::of($customers)
            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('admin.customer.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->make(true);
    }

    public function CustomerDetails($id){
        $cust = User::find($id);

       // $paymentMethods = $cust->paymentMethods();

        // dd($paymentMethods);

        return view('admin.pages.customerdetails')->with([
            'customer' => $cust
        ]);
    }
}
