<?php

namespace App\Http\Controllers\Admin;

use App\Model\Payment\UserPayment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;
use Bitly;
use Yajra\DataTables\DataTables;
use Stripe\Stripe;


class ManagePayment extends Controller
{
    public function CreatePayment(Request $request){

        $uuid =  Uuid::generate()->string;

        $createurl = env('APP_URL').'/process/'.$uuid;

        $url = Bitly::getUrl($createurl); // http://bit.ly/nHcn3

        \Stripe\Stripe::setVerifySslCerts(false);


        //   if ($request->gateway === 2){
            $user = User::find($request->user_id);

        \Stripe\Stripe::setApiKey('sk_test_DdhS6Q7aixO7fojCNlFHO2T300z1Tn2Rve');

        $intent = \Stripe\PaymentIntent::create([
            'amount' => 1099,
            'currency' => 'usd',
            // Verify your integration in this guide by including this parameter
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);

        $ddump = json_encode($intent);

        //return $intent;

       // }


        $a = new UserPayment();
        $a->user_id = $request->user_id;
        $a->amount = $request->amount;
        $a->purpose = $request->purpose;
        $a->gateway = $request->gateway;
        $a->status = 'pending';
        $a->comment = 'pending';
        $a->srturl = $url;
        $a->uuid = $uuid;
        $a->dump = $ddump;
        $a->save();
        return  back();
    }

    public function AllPaymentApi(){
        $customers = UserPayment::select([
            'id',
            'amount',
            'gateway',
            'uuid',
            'status',
            'comment',
            'srturl',

        ])->orderBy('created_at', 'DESC');

        return DataTables::of($customers)
            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('admin.customer.payment.uuid',$invoice->uuid).'" class=" btn btn-xs danger"
                        title="View details"><i class="la la-edit"></i>Delete</a>';
            })


            ->rawColumns(['action'])
            ->make(true);
    }

    public function CustomerDetails($id){


        $cust = User::find($id);

        $paymentMethods = $cust->paymentMethods();

       // dd($paymentMethods);

        return view('admin.pages.customerdetails')->with([
            'customer' => $cust,
            'paymentMethods' => $paymentMethods
        ]);
    }

    public function GetpaymentDetails($uuid){
        $a = UserPayment::where('uuid' , $uuid)->first();
        return $a;
    }
}
