<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use PayPal;
class ManagePayment extends Controller
{
    public function MakePayment(){
        return view('frontend.pages.dashboard');
    }

    public function PayProcess(Request $request){
        $provider = new ExpressCheckout;
        $data = [];
        $data['items'] = [
            [
                'name' => $request->productname,
                'price' => $request->price,
                'desc'  => $request->productname,
        'qty' => 1
    ]
];

$data['invoice_id'] = time();
$data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
$data['return_url'] = url('/payment/success');
$data['cancel_url'] = url('/cart');

$total = 0;
foreach($data['items'] as $item) {
    $total += $item['price']*$item['qty'];
}

$data['total'] = $total;



        $response = $provider->setExpressCheckout($data);



        // This will redirect user to PayPal
        return redirect($response['paypal_link']);

    }


    public function PaySuccess(Request $request) {
        dd($request);
    }






}
