<?php

namespace App\Http\Controllers\Customer;

use App\Model\Payment\UserPayment;
use App\Model\Product\CreateProduct;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;
use Bitly;

class ManageWebPayment extends Controller
{
    public function MakePaymentRequest($id){
        $prod = CreateProduct::find($id);
        return view('frontend.pages.payment')->with([
            'prod' => $prod
        ]);
    }

    public function MakePaymentRequestsend(Request $request){

        $prod = $request->prod_name;
        $description = $request->description;
        $pricing = $request->pricing;

        $uuid =  Uuid::generate()->string;

        $createurl = env('APP_URL').'/process/'.$uuid;

        $url = Bitly::getUrl($createurl); // http://bit.ly/nHcn3

        \Stripe\Stripe::setVerifySslCerts(false);


        //   if ($request->gateway === 2){
      //  $user = User::find($request->user_id);

        $uu = User::where('email',$request->email)->first();


        if ($uu !== null){

            $a = $uu;
        } else {

            $a = new User();
            $a->fname = ' ';
            $a->lname = ' ';
            $a->email = $request->email;
            $a->phone = $request->phone;
            $a->password = bcrypt('password');
            $a->country = 'usa';

            $a->save();
        }






        \Stripe\Stripe::setApiKey('sk_test_DdhS6Q7aixO7fojCNlFHO2T300z1Tn2Rve');

        $intent = \Stripe\PaymentIntent::create([
            'amount' => $pricing*100,
            'currency' => 'usd',
            // Verify your integration in this guide by including this parameter
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);

        $ddump = json_encode($intent);

       // return $a;

        // }


        $b = new UserPayment();
        $b->user_id = $a->id;
        $b->amount = $pricing;
        $b->purpose = $prod;
        $b->gateway = 'h';
        $b->status = 'pending';
        $b->comment = 'pending';
        $b->srturl = $url;
        $b->uuid = $uuid;
        $b->dump = $ddump;
        $b->save();


        return redirect($url);

    }

    public function updatepaymentstatus(Request $request){
        $up = UserPayment::where('uuid',$request->uuid)->first();

        $up->status = 'confirm';
        $up->comment = $request->dump;

        $up->save();

        return response()->json('success',200);


    }








}
