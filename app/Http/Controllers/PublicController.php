<?php

namespace App\Http\Controllers;

use App\Model\Payment\UserPayment;
use App\User;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function GetPayments($uuid){
        $txts = UserPayment::where('uuid', $uuid)->first();
        $user = User::find($txts->user_id);

       $gatewaget = json_decode($txts->dump);

        return view('frontend.pages.publicpay')->with([
            'txts' => $txts,
            'user' => $user,
            'gateway' => $gatewaget,
            'uuid' => $uuid
        ]);
    }

    public function ProcessPayments(Request $request){
        return $request->all();
    }
}
