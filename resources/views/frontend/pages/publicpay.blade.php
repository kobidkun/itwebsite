@extends('frontend.base')
@section('title', 'Pay Now')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <h4 class="title">Pay now</h4>
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a></li>
                            <li>
                                <span class="text-uppercase text-primary font-weight-bold">Pay now</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <style>
        .StripeElement {
            box-sizing: border-box;

            height: 40px;

            padding: 10px 12px;

            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;

            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>

    <!-- Start Contact -->
    <section class="section pb-0">
        <div class="container">
            <div class="row">


                    <div class="section-title ml-lg-12" style="padding-bottom: 222px; width: 100%">

                        <form id="payment-form">
                            <meta name="csrf-token" content="{{ Session::token() }}">

                            <div id="card-element">
                                <!-- Elements will create input elements here -->
                            </div>

                            <!-- We'll put the error messages in this element -->
                            <div id="card-errors" role="alert"></div>

                            <button class="btn btn-primary mt-lg-1 btn-block" id="submit">Pay</button>
                        </form>




                    </div>




            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- End contact -->






@endsection


@section('footer')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script src="https://js.stripe.com/v3/">
    </script>

    <script >
        var stripe = Stripe('pk_test_lU1jwkepQdPATtYzAcNnlQ5i00QzM66cvu');
        var elements = stripe.elements();


    </script>

    <script>
        var style = {
            base: {
                color: "#32325d",
            }
        };

        var card = elements.create("card", { style: style });
        card.mount("#card-element");

        var form = document.getElementById('payment-form');

        form.addEventListener('submit', function(ev) {
            ev.preventDefault();
            stripe.confirmCardPayment('{{$gateway->client_secret}}', {
                payment_method: {
                    card: card,
                    billing_details: {
                        name: 'Jenny Rosen'
                    }
                }
            }).then(function(result) {
                if (result.error) {
                    // Show error to your customer (e.g., insufficient funds)
                    console.log(result.error.message);

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                        footer: '<a href>Either you have Already made payment or you are using invalid card</a>'
                    })
                } else {
                    // The payment has been processed!
                    if (result.paymentIntent.status === 'succeeded') {

                        var rest = JSON.stringify(result.paymentIntent);

                        $.ajaxSetup({

                            headers: {

                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                            }

                        });

                        $.ajax({

                            type:'POST',

                            url: '{{route('frontend.web.pay.update')}}',

                            data:{
                                dump: rest,
                                uuid: '{{$uuid}}',
                            },

                            success:function(data){


                                Swal.fire({
                                    icon: 'success',
                                    title: 'Payment Successfully captured. Shortly you will receive a mail with more details how to get started',
                                    showConfirmButton: false,
                                    timer: 1500
                                })

                            },
                            error:function(data){

                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Something went wrong!',
                                    footer: '<a href>Please Check your  Card Details</a>'
                                })


                            }

                        });









                    }
                }
            });
        });
    </script>


@endsection
