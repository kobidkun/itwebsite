@extends('frontend.base')
@section('title', 'Our Services')
@section('content')

    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <h4 class="title"> Services </h4>
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a>
                            </li>
                            {{--                            <li><a href="#" class="text-uppercase font-weight-bold text-dark">Pages</a></li>--}}
                            <li>
                                <span class="text-uppercase text-primary font-weight-bold">Services</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Feature Start -->
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="features mt-5">
                        <div class="image position-relative d-inline-block">
                            <img src="{{asset('images/icon/code.svg')}}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Website Development</h4>
                            <p class="text-muted mb-0">Web development is the coding or programming that enables website
                                functionality, per the owner's requirements.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{asset('images/icon/cloud.svg')}}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Website Maintenance</h4>
                            <p class="text-muted mb-0">Website maintenance is the act of regularly checking your website
                                for issues and mistakes and keeping it updated and relevant.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{asset('images/icon/buy.svg')}}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">E-Commerce Development</h4>
                            <p class="text-muted mb-0">Ecommerce has completely revolutionized the process of online
                                shopping and has transformed the way consumers purchase their goods and services
                                online.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{asset('images/icon/server.svg')}}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Domain & Server Management</h4>
                            <p class="text-muted mb-0">Domain management, or domain name management, refers to the
                                ongoing tasks of keeping a personal or corporate domain (or domains) stable, secure, and
                                able to support related websites.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{asset('images/icon/find.svg')}}" class="avatar avatar-small" alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Search Engine Optimization</h4>
                            <p class="text-muted mb-0">Search engine optimization (SEO) is the process of increasing the
                                quality and quantity of website traffic by increasing the visibility of a website or a
                                web page to users of a web search engine.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-md-4 col-12 mt-5">
                    <div class="features">
                        <div class="image position-relative d-inline-block">
                            <img src="{{asset('images/icon/digital-marketing.svg')}}" class="avatar avatar-small"
                                 alt="">
                        </div>

                        <div class="content mt-4">
                            <h4 class="title-2">Digital Marketing</h4>
                            <p class="text-muted mb-0">Digital marketing is the component of marketing that utilizes
                                internet and online based digital technologies such as desktop computers, mobile phones
                                and other digital media and platforms to promote products and services</p>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        <div class="container mt-100 mt-60">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-60">
                        <h4 class="title mb-4">Client Reviews</h4>
                        <p class="text-muted para-desc mx-auto mb-0">Start working with <span
                                class="text-primary font-weight-bold">{{config('app.name')}}</span> that can provide
                            everything you need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div id="customer-testi" class="owl-carousel owl-theme">
                        <div class="media customer-testi m-2">
                            <img src="images/client/01.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" To work with the great team, really be attentive and
                                    promptly to respond to the request. Excellent work and I really am pleased with the
                                    results. "</p>
                                <h6 class="text-primary">- Thomas Israel <small class="text-muted">Canada</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="images/client/02.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star-half text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" One day begins with a good conversation. We learn to work
                                    with different teams and bring lots of outputs, which have a lot of handful. Finally
                                    all this is for business. "</p>
                                <h6 class="text-primary">- Barbara McIntosh <small class="text-muted">U.S.A</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="images/client/03.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" The more I asked, the more they gave it! I'm happy to talk
                                    with my website and their representative "</p>
                                <h6 class="text-primary">- Mr. Agarwal <small class="text-muted">India</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="images/client/04.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" They used the difference of time between us for their
                                    benefit and created a very interactive work relationship. They took the time to
                                    understand the requirements. "</p>
                                <h6 class="text-primary">- Anthony Leblanc <small class="text-muted">U.A.E</small></h6>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Feature Start -->

    <!-- Project Start -->
    {{--<section class="section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">Our Latest Projects</h4>
                        <p class="text-muted para-desc mx-auto mb-0">Start working with <span
                                class="text-primary font-weight-bold">{{config('app.name')}}</span> that can provide
                            everything you need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-6 col-md-6 mt-4 pt-2">
                    <div class="work-container position-relative rounded">
                        <img src="images/work/1.jpg" class="img-fluid rounded" alt="work-image">
                        <div class="overlay-work"></div>
                        <div class="content">
                            <a href="javascript:void(0)" class="title text-white d-block font-weight-bold">Shifting
                                Perspective</a>
                            <small class="text-light">Studio</small>
                        </div>
                        <div class="client">
                            <small class="text-light user d-block"><i class="mdi mdi-account"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="mdi mdi-calendar-check"></i> 13th August,
                                2019</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-6 col-md-6 mt-4 pt-2">
                    <div class="work-container position-relative shadow rounded">
                        <img src="images/work/2.jpg" class="img-fluid rounded" alt="work-image">
                        <div class="overlay-work"></div>
                        <div class="content">
                            <a href="javascript:void(0)" class="title text-white d-block font-weight-bold">Colors
                                Magazine</a>
                            <small class="text-light">Web Design</small>
                        </div>
                        <div class="client">
                            <small class="text-light user d-block"><i class="mdi mdi-account"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="mdi mdi-calendar-check"></i> 13th August,
                                2019</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-6 col-md-6 mt-4 pt-2">
                    <div class="work-container position-relative shadow rounded">
                        <img src="images/work/3.jpg" class="img-fluid rounded" alt="work-image">
                        <div class="overlay-work"></div>
                        <div class="content">
                            <a href="javascript:void(0)" class="title text-white d-block font-weight-bold">Spa
                                Cosmetics</a>
                            <small class="text-light">Developing</small>
                        </div>
                        <div class="client">
                            <small class="text-light user d-block"><i class="mdi mdi-account"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="mdi mdi-calendar-check"></i> 13th August,
                                2019</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-6 col-md-6 mt-4 pt-2">
                    <div class="work-container position-relative shadow rounded">
                        <img src="images/work/4.jpg" class="img-fluid rounded" alt="work-image">
                        <div class="overlay-work"></div>
                        <div class="content">
                            <a href="javascript:void(0)" class="title text-white d-block font-weight-bold">Riser
                                Coffee</a>
                            <small class="text-light">Branding</small>
                        </div>
                        <div class="client">
                            <small class="text-light user d-block"><i class="mdi mdi-account"></i> Calvin Carlo</small>
                            <small class="text-light date"><i class="mdi mdi-calendar-check"></i> 13th August,
                                2019</small>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

        --}}{{--<div class="container mt-100 mt-60">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title">
                        <h4 class="title mb-4">See everything about your employee at one place.</h4>
                        <p class="text-muted para-desc mx-auto mb-0">Start working with <span class="text-primary font-weight-bold">{{config('app.name')}}</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>

                        <div class="mt-3">
                            <a href="javascript:void(0)" class="btn btn-primary mt-2 mr-2">Get Started Now</a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary mt-2">Free Trial</a>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->--}}{{--
    </section><!--end section-->--}}
    <!-- Project End -->
    @include('frontend.component.contact-form')
@endsection
