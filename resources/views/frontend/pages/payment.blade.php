@extends('frontend.base')
@section('title', 'Search Engine Optimization')
@section('content')

    <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Pricing Details</h4>
                  </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-lg-5 col-md-6 col-12 mt-4 pt-2">
                <div class="rounded border bg-light shadow">
                    <div class="bg-primary p-4 text-center rounded-top">
                        <h4 class="mb-0 title-dark text-light">Payment Details</h4>
                    </div>

                    <div class="p-4">
                        <div class="d-flex justify-content-center mb-4">
                            <span class="price text-primary font-weight-bold display-4 mb-0">{{$prod->pricing}}</span>
                            <span class="h4 mb-0 mt-2 text-primary">USD</span>

                        </div>

                        <ul class="feature list-inline">
                            <li class="h5 font-weight-normal"><i class="mdi mdi-check-decagram text-primary mr-2"></i>{{$prod->name}}</li>
                            <li class="h5 font-weight-normal"><i class="mdi mdi-check-decagram text-primary mr-2"></i>{{$prod->description}}</li>
                            <li class="h5 font-weight-normal"><i class="mdi mdi-check-decagram text-primary mr-2"></i>24×7 support</li>
                        </ul>

                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-7 col-md-6 col-12 mt-4 pt-2">
                <div class="rounded bg-white border shadow">
                    <div class="bg-light p-4 text-center rounded-top">
                        <h4 class="mb-0 text-primary">Add Your Information</h4>
                    </div>

                    <div class="p-4">
                        <form action="{{route('frontend.web.pay.send')}}"  method="post"  >
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name : </label>
                                        <input name="name" id="name" type="text" class="form-control font-weight-bold" r
                                               required placeholder="Name">
                                    </div>
                                </div><!--end col-->

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Your Email : </label>
                                        <input name="email" id="name" type="email"
                                               class="form-control font-weight-bold" required placeholder="Name">
                                    </div>
                                </div><!--end col-->


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Your Phone : </label>
                                        <input name="phone" id="name" type="number" class="form-control font-weight-bold"
                                               required placeholder="Name">
                                    </div>
                                </div><!--end col-->


                                <input type="hidden" value="{{$prod->pricing}}" name="pricing">
                                <input type="hidden" value="{{$prod->description}}" name="description">
                                <input type="hidden" value="{{$prod->name}}" name="prod_name">


                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="submit" id="submit" name="send" class="submitBnt btn btn-primary" value="Proceed">
                                </div><!--end col-->
                            </div><!--end row-->

                            </div>

                        </form><!--end form-->
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
    <!-- Payment and Price End -->
    </section><!--end section-->

    @endsection
