@extends('frontend.base')
@section('title', 'Contact Us')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a></li>
                            <li>
                                <span class="text-uppercase text-primary font-weight-bold">Dashboard</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Start Contact -->
    <section class="section pb-0">
        <div class="container">
            <div class="row">

                <div class="p-4">
                    <form action="{{route('frontend.payprocess')}}" method="POST" >
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label>Product Name <span class="text-danger">*</span></label>
                                    <i class="mdi mdi-account ml-3 icons"></i>
                                    <input name="productname"
                                           required
                                           type="text" class="form-control pl-5" placeholder="Product Name">
                                </div>
                            </div><!--end col-->


                            <div class="col-md-6">
                                <div class="form-group position-relative">
                                    <label>Price
                                    </label>
                                    <i class="mdi mdi-email ml-3 icons"></i>
                                    <input name="price"  type="number" required class="form-control pl-5" placeholder="Price">
                                </div>
                            </div><!--end col-->



                        </div><!--end row-->
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="submit" id="submit" name="send" class="btn btn-primary" value="Submit">
                            </div><!--end col-->
                        </div><!--end row-->
                    </form><!--end form-->
                </div>




            </div><!--end row-->
        </div><!--end container-->



    </section><!--end section-->
    <!-- End contact -->

@endsection
