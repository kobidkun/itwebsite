@extends('frontend.base')
@section('title', 'About Us')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <h4 class="title"> About us </h4>
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a>
                            </li>
                            {{--                            <li><a href="#" class="text-uppercase font-weight-bold text-dark">Pages</a></li>--}}
                            <li>
                                <span class="text-uppercase text-primary font-weight-bold">About</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- About Start -->
    <section class="section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-5 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="position-relative">
                        <img src="images/about.jpg" class="rounded img-fluid mx-auto d-block" alt="">
                        {{--<div class="play-icon">
                            <a href="http://vimeo.com/287684225" class="play-btn video-play-icon">
                                <i class="mdi mdi-play text-primary rounded-pill bg-white shadow"></i>
                            </a>
                        </div>--}}
                    </div>
                </div><!--end col-->

                <div class="col-lg-7 col-md-7 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <div class="section-title ml-lg-4">
                        <h4 class="title mb-4">Our Story</h4>
                        <p class="text-muted">Since our founding in 1997, <span
                                class="text-primary font-weight-bold">{{config('app.name')}}</span> has worked as an IT
                            partner to organizations to develop and enhance their mission-critical business systems.</p>
                        <p class="text-muted">We provide custom software applications, solve data management problems,
                            and support the evolution of the mobile workforce. As your Information Technology partner,
                            we transition you from systems and processes that aren’t working, to custom IT services that
                            help you save money, improve performance, and work more efficiently. We define the solution
                            by starting with your business needs.</p>
                        <p class="text-muted">At {{config('app.name')}}, we take a business-focused approach to our
                            projects, using technology as a means to
                            solving a problem rather than pushing a predefined tool. In developing effective, modern,
                            applications, we account for the problems that our clients seek to overcome, as well as the
                            goals they wish to achieve. Our emphasis on forming IT partnerships with our customers is
                            the cornerstone of the longstanding relationships we’ve enjoyed during our corporate
                            history.</p>
                        <a href="{{route('frontend.contact')}}" class="btn btn-primary mt-3">Let's Start with your idea
                            <i
                                class="mdi mdi-chevron-right"></i></a>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- About End -->
    @include('frontend.component.key_features')
    <!-- Team Start -->
    <section class="section bg-light">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">Our Greatest Minds</h4>
                        <p class="text-muted para-desc mx-auto mb-0">We pride ourselves with the people
                            in our team and we believe it is the one thing that makes our company special.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="team text-center">
                        <div class="position-relative">
                            <img src="images/client/01.jpg" class="img-fluid avatar avatar-ex-large rounded-pill"
                                 alt="">
                            <ul class="list-unstyled social-icon team-icon mb-0 mt-4">
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-facebook" title="Facebook"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-instagram" title="Instagram"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-twitter" title="Twitter"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-google-plus" title="Twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="content pt-3 pb-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Ronny Jofra</a></h5>
                            <small class="designation text-muted">C.E.O</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="team text-center">
                        <div class="position-relative">
                            <img src="images/client/04.jpg" class="img-fluid avatar avatar-ex-large rounded-pill"
                                 alt="">
                            <ul class="list-unstyled social-icon team-icon mb-0 mt-4">
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-facebook" title="Facebook"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-instagram" title="Instagram"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-twitter" title="Twitter"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-google-plus" title="Twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="content pt-3 pb-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Micheal Carlo</a></h5>
                            <small class="designation text-muted">Director</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="team text-center">
                        <div class="position-relative">
                            <img src="images/client/02.jpg" class="img-fluid avatar avatar-ex-large rounded-pill"
                                 alt="">
                            <ul class="list-unstyled social-icon team-icon mb-0 mt-4">
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-facebook" title="Facebook"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-instagram" title="Instagram"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-twitter" title="Twitter"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-google-plus" title="Twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="content pt-3 pb-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Aliana Rosy</a></h5>
                            <small class="designation text-muted">Manager</small>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="team text-center">
                        <div class="position-relative">
                            <img src="images/client/03.jpg" class="img-fluid avatar avatar-ex-large rounded-pill"
                                 alt="">
                            <ul class="list-unstyled social-icon team-icon mb-0 mt-4">
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-facebook" title="Facebook"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-instagram" title="Instagram"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-twitter" title="Twitter"></i></a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i
                                            class="mdi mdi-google-plus" title="Twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="content pt-3 pb-3">
                            <h5 class="mb-0"><a href="javascript:void(0)" class="name text-dark">Sofia Razaq</a></h5>
                            <small class="designation text-muted">Developer</small>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

    </section><!--end section-->
    <!-- Team End -->
    @include('frontend.component.contact-form')
@endsection
