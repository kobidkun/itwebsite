@extends('frontend.base')
@section('title', 'E-Commerce Development')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <img src="{{asset('images/icon/buy.svg')}}" class="avatar avatar-small" alt="">
                        <h4 class="title mt-4 mb-3"> E-Commerce Development </h4>
                        <p class="para-desc mx-auto text-muted">Ecommerce has completely revolutionized the process of
                            online
                            shopping and has transformed the way consumers purchase their goods and services
                            online.</p>
                        {{--<ul class="list-unstyled">
                            <li class="list-inline-item text-primary mr-3"><i class="mdi mdi-map-marker text-warning mr-2"></i>Beijing, China</li>
                            <li class="list-inline-item text-primary"><i class="mdi mdi-office-building text-warning mr-2"></i>Gradle</li>
                        </ul>--}}
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a>
                            </li>
                            <li><a href="{{route('frontend.services')}}"
                                   class="text-uppercase font-weight-bold text-dark">Services</a></li>
                            <li>
                                <span
                                    class="text-uppercase text-primary font-weight-bold">E-Commerce Development</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Job Detail Start -->
    <section class="section">
        <div class="container">
            <div class="row">


                <div class="col-lg-8 col-md-7 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="ml-lg-4">
                        <h5>About Our E-Commerce Development Service: </h5>
                        <p class="text-muted">Ecommerce has completely revolutionized the process of online shopping and
                            has transformed the way consumers purchase their goods and services online. It helps buyers,
                            sellers and end users to connect to each other irrespective of their geographical presence
                            and offers the right platform to endorse your goods and services online.</p>
                        <p class="text-muted"><span class="text-primary font-weight-bold">{{config('app.name')}}</span> provides eCommerce web development services to the clients with
                            the best software and development plans for their unique requirements. We have a dedicated
                            team of expert consultants, developers and project managers to ensure that our customers not
                            only receive a successful development process but also a collaborative strategic
                            partner.</p>
                        <p class="text-muted">Our ECommerce Development Portfolio and Capability includes:</p>
                        <ul class="list-unstyled">
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Maintenance
                                and Support
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Configuration
                                and Installation of Shopping Cart Software
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Secure
                                Payment Gateway Integration
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i> B2B and
                                B2C ECommerce Website Development Company
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Custom
                                ECommerce Website Development
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>
                                ECommerce Shopping Cart Development
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i> E-Bay
                                Integration
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i> Adding
                                Functionality or Extending the Capability of an Existing Online Store
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>ECommerce
                                Application Development
                            </li>
                        </ul>
                        <p class="text-muted">The custom eCommerce development from {{config('app.name')}} ensures that your website
                            is built with a sound business strategy and the latest cutting edge development
                            technologies. Being a leading ecommerce development company Our ecommerce websites
                            development solution delivers a comprehensive and effective e-business strategy, products
                            and marketing, design and usability, technology and security to construct an interactive
                            ecommerce website and a communicating store for every business.</p>
                        <p class="text-muted">Our highly functional and simple to use ecommerce web designs stand out
                            from the crowd and enables your customers to easily buy from you.</p>
                        <p class="text-muted">Our team of expert web developers have an extensive knowledge base of
                            building online stores using technologies like 3D Cart, AgoraCart, BigCommerce, CafePress!,
                            CubeCart, Drupal, Joomla, Foxycart, Magento, osCommerce, PrestaShop, Shopify, SilverStripe,
                            VirtueMart, WordPress, X-Cart, Yahoo! Merchant Solutions, Zen Cart, NET, PHP etc have
                            successfully delivered an engaging online experience to the customers and maximised their
                            conversions. Our ecommerce website development solutions are flexible, scalable and offer
                            faster time-to-market and build essential customer traffic.</p>

                        <div class="mt-4">
                            <a href="{{route('frontend.contact')}}" class="btn btn-outline-primary">Let's Start <i
                                    class="mdi mdi-send"></i></a>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-4 col-md-5 col-12">
                    <div class="sidebar rounded shadow">
                        <div class="widget border-bottom p-4">
                            <h5 class="mb-0">Service Information</h5>
                        </div>

                        <div class="p-4">
                            <div class="widget">
                                <i class="mdi mdi-account-check mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Service Type:</h4>
                                    <p class="text-primary">Development</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-calendar-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Experience in Field:</h4>
                                    <p class="text-primary">8+ Years</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-monitor mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Clients:</h4>
                                    <p class="text-primary">300</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-briefcase-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Sales:</h4>
                                    <p class="text-primary">500</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Job Detail End -->

    <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Pricing </h4>
                    <p class="text-muted para-desc mb-0 mx-auto">Lets get started with your new  <span class="text-primary font-weight-bold">Website </span>
                        and boost your business to next level.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row align-items-center">
            <div class="col-12 mt-4 pt-2">
                <div class="text-center">
                    <ul class="nav nav-pills rounded-pill justify-content-center d-inline-block border py-1 px-2" id="pills-tab" role="tablist">
                        <li class="nav-item d-inline-block">
                            <a class="nav-link px-3 rounded-pill active monthly" id="Monthly" data-toggle="pill" href="#Month"
                               role="tab" aria-controls="Month" aria-selected="true">One Time Charge</a>
                        </li>

                    </ul>
                </div>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="Month" role="tabpanel" aria-labelledby="Monthly">
                        <div class="row">


                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">WooCommerce</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">299</span>
                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Server</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Domain</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Free SSL</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>15 business Email</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Source Files</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>6 month free support</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Dynamic Product page  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Dynamic Admin page  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Payment Gateway  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>SMS Gateway  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Product Cart  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Billing & Invoicing  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Product Tracking  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>ERP intrigation via API  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>3 Changes  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>CRM to manage products  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Customisation / Page Addition $50/hour  </li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Mangento Development</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">799</span>
                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Server</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Domain</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Free SSL</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>15 business Email</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Source Files</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>6 month free support</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Dynamic Product page  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Dynamic Admin page  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Payment Gateway  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>SMS Gateway  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Product Cart  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Billing & Invoicing  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Product Tracking  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>ERP intrigation via API  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>3 Changes  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>CRM to manage products  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Customisation / Page Addition $50/hour  </li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Customised Ecommerce</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">1099</span>
                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Server</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Domain</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Free SSL</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>15 business Email</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Source Files</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>6 month free support</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Dynamic Product page  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Dynamic Admin page  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Payment Gateway  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>SMS Gateway  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Product Cart  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Billing & Invoicing  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Product Tracking  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>ERP intrigation via API  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>3 Changes  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>CRM to manage products  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Customisation / Page Addition $50/hour  </li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Customised Website</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">2099</span>

                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Customised web develop</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>100 Man hours included </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Source Files</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Domain Free</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Cost @ $50/hour  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Server</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>1 Year free Domain</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Free SSL</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>15 business Email</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Source Files</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>6 month free support</li>
                                        <li class="feature-list"><i class=" text-success h5 mr-1"></i> * This is booking advance. Total price and invoice will be sent via Email
                                            upon project completion
                                        </li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Started Now</a>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div>

                    ** Project Tracking Tool will be provided within 3 Business days.Please Check your email for more details.


                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

@stop
