@extends('frontend.base')
@section('title', 'Domain and Server Maintenance')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <img src="{{asset('images/icon/server.svg')}}" class="avatar avatar-small" alt="">
                        <h4 class="title mt-4 mb-3"> Domain and Server Maintenance </h4>
                        <p class="para-desc mx-auto text-muted">Domain management, or domain name management, refers to
                            the
                            ongoing tasks of keeping a personal or corporate domain (or domains) stable, secure, and
                            able to support related websites.</p>
                        {{--<ul class="list-unstyled">
                            <li class="list-inline-item text-primary mr-3"><i class="mdi mdi-map-marker text-warning mr-2"></i>Beijing, China</li>
                            <li class="list-inline-item text-primary"><i class="mdi mdi-office-building text-warning mr-2"></i>Gradle</li>
                        </ul>--}}
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a>
                            </li>
                            <li><a href="{{route('frontend.services')}}"
                                   class="text-uppercase font-weight-bold text-dark">Services</a></li>
                            <li>
                                <span
                                    class="text-uppercase text-primary font-weight-bold">Domain and Server Maintenance</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Job Detail Start -->
    <section class="section">
        <div class="container">
            <div class="row">


                <div class="col-lg-8 col-md-7 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="ml-lg-4">
                        <h5>About Our Domain and Server Maintenance Service: </h5>
                        <p class="text-muted">As a global provider of network and telecommunication equipment and
                            services, we understand why IT server management needs to be a priority for any industry.
                            That’s why we’re committed to helping our clients keep their servers running to avoid any
                            crashes. When there’s a problem, we’ll be there right away.</p>
                        <p class="text-muted">Our IT server maintenance plan includes a variety of steps, such as:</p>
                        <ul class="list-unstyled">
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Checking your server log files</li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Checking your hard disk space</li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Examining any folder permissions</li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Examining security features</li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Installing security software patches</li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Updating antivirus software</li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Performing regular comprehensive back-ups</li>
                        </ul>
                        <p class="text-muted">This plan will keep your server software updated and your computer network
                            running smoothly. Our focus on security — such as installing security software patches and
                            antivirus software — will protect your data, while preventive maintenance such as checking
                            your hard disk space and folder permissions will help you avoid software crashes and network
                            failure. We’ll also back your data up at regular intervals, so if something fails, you can
                            rely on your IT server management partner, Worldwide Services.</p>

                        <div class="mt-4">
                            <a href="{{route('frontend.contact')}}" class="btn btn-outline-primary">Let's Start <i
                                    class="mdi mdi-send"></i></a>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-4 col-md-5 col-12">
                    <div class="sidebar rounded shadow">
                        <div class="widget border-bottom p-4">
                            <h5 class="mb-0">Service Information</h5>
                        </div>

                        <div class="p-4">
                            <div class="widget">
                                <i class="mdi mdi-account-check mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Service Type:</h4>
                                    <p class="text-primary">Server Maintenance</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-calendar-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Experience in Field:</h4>
                                    <p class="text-primary">10+ Years</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-monitor mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Clients:</h4>
                                    <p class="text-primary">600</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-briefcase-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Sales:</h4>
                                    <p class="text-primary">750</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Job Detail End -->



@stop
