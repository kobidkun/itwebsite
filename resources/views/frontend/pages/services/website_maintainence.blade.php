@extends('frontend.base')
@section('title', 'Website Maintenance')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <img src="{{asset('images/icon/digital-marketing.svg')}}" class="avatar avatar-small" alt="">
                        <h4 class="title mt-4 mb-3"> Website Maintenance </h4>
                        <p class="para-desc mx-auto text-muted">Website maintenance is the act of regularly checking
                            your website for issues and mistakes and keeping it updated and relevant.</p>
                        {{--<ul class="list-unstyled">
                            <li class="list-inline-item text-primary mr-3"><i class="mdi mdi-map-marker text-warning mr-2"></i>Beijing, China</li>
                            <li class="list-inline-item text-primary"><i class="mdi mdi-office-building text-warning mr-2"></i>Gradle</li>
                        </ul>--}}
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a>
                            </li>
                            <li><a href="{{route('frontend.services')}}"
                                   class="text-uppercase font-weight-bold text-dark">Services</a></li>
                            <li>
                                <span class="text-uppercase text-primary font-weight-bold">Website Maintenance</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Job Detail Start -->
    <section class="section">
        <div class="container">
            <div class="row">


                <div class="col-lg-8 col-md-7 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="ml-lg-4">
                        <h5>About Our Website Maintenance Service: </h5>
                        <p class="text-muted">With every online search, 50 percent of users discover a new company,
                            product, or service. They visit your website, explore your products, browse your services,
                            and get a first impression of your business. That first impression can make or break your
                            company’s next sale.</p>
                        <p class="text-muted">For businesses today, this fact makes website maintenance a top
                            priority.</p>
                        <p class="text-muted">As your partner, {{config('app.name')}} provides your company with a custom and
                            comprehensive website maintenance plan that helps your business deliver a fast, secure, and
                            seamless online experience. Plus, as a full-service digital marketing agency, we offer
                            turnkey solutions for maximizing your site’s performance.</p>
                        <p class="text-muted">Learn how our award-winning team of developers, designers, and digital
                            marketers can maintain and improve your website by browsing our web maintenance service
                            plans and prices below. Or, contact us online to tell us about your business.</p>

                        <div class="mt-4">
                            <a href="{{route('frontend.contact')}}" class="btn btn-outline-primary">Let's Start <i
                                    class="mdi mdi-send"></i></a>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-4 col-md-5 col-12">
                    <div class="sidebar rounded shadow">
                        <div class="widget border-bottom p-4">
                            <h5 class="mb-0">Service Information</h5>
                        </div>

                        <div class="p-4">
                            <div class="widget">
                                <i class="mdi mdi-account-check mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Service Type:</h4>
                                    <p class="text-primary">Maintenance</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-calendar-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Experience in Field:</h4>
                                    <p class="text-primary">12+ Years</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-monitor mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Clients:</h4>
                                    <p class="text-primary">500</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-briefcase-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Sales:</h4>
                                    <p class="text-primary">1200</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Job Detail End -->

    <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Pricing </h4>
                    <p class="text-muted para-desc mb-0 mx-auto">Lets get started with your new  <span class="text-primary font-weight-bold">Website </span>
                        and boost your business to next level.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row align-items-center">
            <div class="col-12 mt-4 pt-2">
                <div class="text-center">
                    <ul class="nav nav-pills rounded-pill justify-content-center d-inline-block border py-1 px-2" id="pills-tab" role="tablist">
                        <li class="nav-item d-inline-block">
                            <a class="nav-link px-3 rounded-pill active monthly" id="Monthly" data-toggle="pill" href="#Month"
                               role="tab" aria-controls="Month" aria-selected="true">Yearly</a>
                        </li>

                    </ul>
                </div>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="Month" role="tabpanel" aria-labelledby="Monthly">
                        <div class="row">


                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Bronze</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">499</span>
                                        <span class="h4 mb-0 mt-2">/Y</span>

                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Time Allocated each Month upto 5 Hours</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Setup Fee $0</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Work Report</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Updated Timeframe in 2-3 working days</li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Silver</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">599</span>
                                        <span class="h4 mb-0 mt-2">/Y</span>

                                    </div>

                                    <ul class="feature list-unstyled pl-0">

                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Time Allocated each Month upto 10 Hours</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Setup Fee $0</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Work Report</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Updated Timeframe in 2-3 working days</li>                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Gold</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">799</span>
                                        <span class="h4 mb-0 mt-2">/Y</span>

                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Time Allocated each Month upto 20 Hours</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Setup Fee $0</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Work Report</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Updated Timeframe in 2-3 working days</li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Platinum</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">1099</span>
                                        <span class="h4 mb-0 mt-2">/Y</span>


                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Time Allocated each Month upto 40 Hours</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Setup Fee $0</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Work Report</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Updated Timeframe in 2-3 working days</li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Started Now</a>
                                </div>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div>

                    ** Project Tracking Tool will be provided within 3 Business days.Please Check your email for more details.


                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
@stop
