@extends('frontend.base')
@section('title', 'Search Engine Optimization')
@section('content')
    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="page-next-level">
                        <img src="{{asset('images/icon/find.svg')}}" class="avatar avatar-small" alt="">
                        <h4 class="title mt-4 mb-3"> Search Engine Optimization </h4>
                        <p class="para-desc mx-auto text-muted">Search engine optimization (SEO) is the process of
                            increasing the
                            quality and quantity of website traffic by increasing the visibility of a website or a
                            web page to users of a web search engine.</p>
                        {{--<ul class="list-unstyled">
                            <li class="list-inline-item text-primary mr-3"><i class="mdi mdi-map-marker text-warning mr-2"></i>Beijing, China</li>
                            <li class="list-inline-item text-primary"><i class="mdi mdi-office-building text-warning mr-2"></i>Gradle</li>
                        </ul>--}}
                        <ul class="page-next d-inline-block bg-white shadow p-2 pl-4 pr-4 rounded mb-0">
                            <li><a href="{{route('frontend.index')}}" class="text-uppercase font-weight-bold text-dark">Home</a>
                            </li>
                            <li><a href="{{route('frontend.services')}}"
                                   class="text-uppercase font-weight-bold text-dark">Services</a></li>
                            <li>
                                <span
                                    class="text-uppercase text-primary font-weight-bold">Search Engine Optimization</span>
                            </li>
                        </ul>
                    </div>
                </div>  <!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->
    <!-- Hero End -->

    <!-- Shape Start -->
    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!--Shape End-->

    <!-- Job Detail Start -->
    <section class="section">
        <div class="container">
            <div class="row">


                <div class="col-lg-8 col-md-7 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                    <div class="ml-lg-4">
                        <h5>About Our Search Engine Optimization Service: </h5>
                        <p class="text-muted">Most people use the web to find products, services, and businesses. Are
                            you on the first page of the search engine results for the keywords that match your business
                            or brand? If not, you're missing out on valuable leads.</p>
                        <p class="text-muted">Search Engine Optimization (SEO) isn't a fad, and it's no longer an option
                            to simply not think about it if you want to grow your business. It's an indispensable part
                            of any plan to market your products or services and bring in more qualified leads and
                            sales.</p>
                        <p class="text-muted">At {{config('app.name')}}, we believe that our job is to make a technical, complicated,
                            and sometimes overwhelming internet marketing process as simple as possible for our clients.
                            One of the many ways we accomplish this is by providing each of our SEO clients with a
                            single point of contact to manage your project and your questions. You will get to know your
                            {{config('app.name')}} SEO Professional well and they will get to know your business and understand your
                            unique needs. We will become your outsourced online marketing department.</p>
                        <p class="text-muted">At {{config('app.name')}}, we are building a culture of extreme customer focus. John
                            Spence, in his book Awesomely Simple, says, “the only sustainable competitive differentiator
                            left to most businesses today is creating a culture of continuous innovation and extreme
                            customer focus driven by highly talented people.” We believe in owning the voice of our
                            clients and work hard to show it.</p>
                        <p class="text-muted">While our competitors use smoke and mirrors, we maintain full transparency
                            so you can see first-hand how our efforts are increasing your online revenue. Our custom
                            in-depth reporting measures include:</p>
                        <ul class="list-unstyled">
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>24/7
                                access to an online client portal.
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Google
                                Analytics reports that give a full overview of your site’s SEO performance.
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Regularly
                                updated keyword rankings records that you can check anytime you want.
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i> Monthly
                                breakdown of the SEO tasks we’ve performed and the hours we’ve spent to help you monitor
                                your ROI.
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>Monthly
                                video summary from your SEO Specialist reviewing the month’s SEO improvements and
                                strategy.
                            </li>
                            <li class="text-muted"><i class="mdi mdi-pan-right text-success mdi-18px mr-2"></i>
                                SEO professionals who are available by email or phone to respond to any questions or
                                discussions you would like to have.
                            </li>
                        </ul>

                        <div class="mt-4">
                            <a href="{{route('frontend.contact')}}" class="btn btn-outline-primary">Let's Start <i
                                    class="mdi mdi-send"></i></a>
                        </div>
                    </div>
                </div><!--end col-->
                <div class="col-lg-4 col-md-5 col-12">
                    <div class="sidebar rounded shadow">
                        <div class="widget border-bottom p-4">
                            <h5 class="mb-0">Service Information</h5>
                        </div>

                        <div class="p-4">
                            <div class="widget">
                                <i class="mdi mdi-account-check mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Service Type:</h4>
                                    <p class="text-primary">Optimization</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-calendar-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Experience in Field:</h4>
                                    <p class="text-primary">6+ Years</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-monitor mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Clients:</h4>
                                    <p class="text-primary">900</p>
                                </div>
                            </div>
                            <div class="widget">
                                <i class="mdi mdi-briefcase-outline mdi-24px float-left mr-3"></i>
                                <div class="overflow-hidden d-block">
                                    <h4 class="widget-title mb-0">Total Sales:</h4>
                                    <p class="text-primary">2000</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Job Detail End -->

    <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Pricing </h4>
                    <p class="text-muted para-desc mb-0 mx-auto">Lets get started with your new  <span class="text-primary font-weight-bold">Website </span>
                        and boost your business to next level.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row align-items-center">
            <div class="col-12 mt-4 pt-2">
                <div class="text-center">
                    <ul class="nav nav-pills rounded-pill justify-content-center d-inline-block border py-1 px-2" id="pills-tab" role="tablist">
                        <li class="nav-item d-inline-block">
                            <a class="nav-link px-3 rounded-pill active monthly" id="Monthly" data-toggle="pill" href="#Month"
                               role="tab" aria-controls="Month" aria-selected="true">One Time Charge</a>
                        </li>

                    </ul>
                </div>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="Month" role="tabpanel" aria-labelledby="Monthly">
                        <div class="row">


                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Basic SEO</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">149</span>
                                        <span class="h4 mb-0 mt-2">/M</span>

                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>In-Depth Site Analysis	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Duplicate Content Check	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Competition Analysis	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Keyword Analysis	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Baseline Ranking Check	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Keyword URL Mapping	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Broken Links Check	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Google Penalty Check	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Canonicalization  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Header (H1) Tags Optimization	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Internal Link Structuring & Optimization	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Xml Sitemap/Analysis	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Fresh Web Content Suggestions (Writing charges extra)	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Image & Hyperlink Optimization	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Schema on Contact Address	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Google My Business Setup & Verification	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Customer Reviews/Ratings Submissions	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Blog Writing		  </li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->

                            <div class="col-lg-3 col-md-6 col-12 mt-4 pt-2">
                                <div class="pricing-rates business-rate shadow bg-light p-4 rounded">
                                    <h2 class="title text-uppercase mb-4">Advance SEO</h2>
                                    <div class="d-flex mb-4">
                                        <span class="h4 mb-0 mt-2">$</span>
                                        <span class="price h1 mb-0">299</span>
                                        <span class="h4 mb-0 mt-2">/M</span>

                                    </div>

                                    <ul class="feature list-unstyled pl-0">
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>In-Depth Site Analysis	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Duplicate Content Check	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Competition Analysis	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Keyword Analysis	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Baseline Ranking Check	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Keyword URL Mapping	</li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Broken Links Check	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Google Penalty Check	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Canonicalization  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Header (H1) Tags Optimization	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Internal Link Structuring & Optimization	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Xml Sitemap/Analysis	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Fresh Web Content Suggestions (Writing charges extra)	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Image & Hyperlink Optimization	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Schema on Contact Address	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Google My Business Setup & Verification	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Customer Reviews/Ratings Submissions	  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Blog Writing		  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Blog Submission			  </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Infographic Distribution	 </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Micro Blogging	 </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Twitter Page Optimization		 </li>
                                        <li class="feature-list"><i class="mdi mdi-check text-success h5 mr-1"></i>Search Engine Rank Report</li>
                                    </ul>
                                    <a href="javascript:void(0)" class="btn btn-primary mt-4">Buy Now</a>
                                </div>
                            </div><!--end col-->




                        </div><!--end row-->
                    </div>

                    ** Project Tracking Tool will be provided within 3 Business days.Please Check your email for more details.


                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
@stop
