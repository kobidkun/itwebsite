
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="The best IT Company in the World, Probably!" />
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
    <meta name="author" content="Shreethemes" />
    <meta name="Version" content="2.0" />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{asset('/images/logo/fab.png')}}">
    <!-- Bootstrap -->
    <link href="{{asset('/generated/style.min.css')}}" rel="stylesheet" type="text/css" />


</head>

<body>
<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
</div>
<!-- Loader -->

@include('frontend.component.header')

@yield('content')

@include('frontend.component.footer')

<!-- Back to top -->
<a href="#" class="back-to-top rounded text-center" id="back-to-top">
    <i class="mdi mdi-chevron-up d-block"></i>
</a>
<!-- Back to top -->

<!-- javascript -->
<script src="{{asset('/generated/script.min.js')}}"></script>


@yield('footer')
</body>

</html>
