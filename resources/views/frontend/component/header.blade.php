<!-- Navbar STart -->
<header id="topnav" class="defaultscroll sticky">
    <div class="container">
        <!-- Logo container-->
        <div>
            <a class="logo" href="{{route('frontend.index')}}">
                <img src="{{asset('/images/logo/logo.png')}}" width="122px" alt="">
            </a>
        </div>
        <div class="buy-button">
            @if (Route::has('login'))
                @auth
                    <a href="{{route('login')}}" class="btn btn-primary"><i class="mdi mdi-account-arrow-right"></i>My
                        Profile</a>
                @else
                    <a href="{{route('login')}}" class="btn btn-primary"><i class="mdi mdi-account-check"></i>Login</a>
                @endauth
            @endif
        </div><!--end login button-->
        <!-- End Logo container-->
        <div class="menu-extras">
            <div class="menu-item">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </div>
        </div>

        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                <li><a href="{{route('frontend.index')}}">Home</a></li>
                <li class="has-submenu">
                    <a href="{{route('frontend.services')}}"> Services</a><span class="menu-arrow"></span>
                    <ul class="submenu">
                        <li><a href="{{route('frontend.service.webdev')}}">Website Development</a></li>
                        <li><a href="{{route('frontend.service.webmaintain')}}">Website Maintenance</a></li>
                        <li><a href="{{route('frontend.service.ecommerce')}}">E-Commerce Development</a></li>
                        <li><a href="{{route('frontend.service.domainserver')}}">Domain & Server Management</a></li>
                        <li><a href="{{route('frontend.service.seo')}}">Search Engine Optimization</a></li>
                        <li><a href="{{route('frontend.service.digimart')}}">Digital Marketing</a></li>
                        <li><a href="{{route('frontend.services')}}">All Services</a><span class="submenu-arrow"></span></li>
                    </ul>
                </li>
                <li><a href="{{route('frontend.about')}}">About Us</a></li>
                <li><a href="{{route('frontend.contact')}}">Contact Us</a></li>
            </ul><!--end navigation menu-->
            <div class="buy-menu-btn d-none">
                @if (Route::has('login'))
                    @auth
                        <a href="{{route('login')}}" class="btn btn-primary"><i class="mdi mdi-account-arrow-right"></i>My
                            Profile</a>
                    @else
                        <a href="{{route('login')}}" class="btn btn-primary"><i class="mdi mdi-account-check"></i>Login</a>
                    @endauth
                @endif
            </div><!--end login button-->
        </div><!--end navigation-->
    </div><!--end container-->
</header><!--end header-->
<!-- Navbar End -->
