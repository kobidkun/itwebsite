<!-- Key Feature Start -->
<section class="section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Our Services</h4>
                    <p class="text-muted para-desc mx-auto mb-0">From client engagement to technology implementation to
                        service delivery, the IT Solutions and Services of <span class="text-primary font-weight-bold">{{config('app.name')}}</span> give you access to our proficiency in advanced
                        technologies and proven practices along the entire IT life cycle.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row">
            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-responsive text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Website Development</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-internet-explorer text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Website Maintenance</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-cart text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">E-Commerce Development</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-domain text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Domain Management</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-server text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Server Management</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-search-web text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Search Engine Optimization</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-check-decagram text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Digital Marketing</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-facebook text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Facebook Ads</h4>
                    </div>
                </div>
            </div><!--end col-->

            <div class="col-lg-4 col-md-6 mt-4 pt-2">
                <div class="key-feature d-flex p-3 bg-white rounded shadow">
                    <div class="icon text-center rounded-pill mr-3">
                        <i class="mdi mdi-google-adwords text-primary"></i>
                    </div>
                    <div class="content mt-2">
                        <h4 class="title mb-0">Google ads</h4>
                    </div>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <div class="row justify-content-center">
            <div class="col-12 text-center mt-4 pt-2">
                <a href="{{route('frontend.services')}}" class="btn btn-primary">See More <i
                        class="mdi mdi-arrow-right"></i></a>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->
</section><!--end section-->
<!-- Key Feature End -->
