<!-- Shape Start -->
<div class="position-relative">
    <div class="shape overflow-hidden text-white">
        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
        </svg>
    </div>
</div>
<!--Shape End-->

<!-- Start Contact -->
<section class="section pb-0">
    <div class="container mt-100 mt-60">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <div class="section-title mb-4 pb-2">
                    <h4 class="title mb-4">Get a Call Back</h4>
                    <p class="text-muted para-desc mx-auto mb-0">With our ability and transparent pricing model, we will
                        entertain your inquiry in the best possible way to give a satisfactory reply.</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0 order-2 order-md-1">
                <div class="pt-5 pb-5 p-4 bg-light shadow rounded">
                    <h4>Get In Touch !</h4>
                    <div class="custom-form mt-4">
                        <div id="message"></div>
                        <form method="post" action="{{route('frontend.contact.post')}}" name="contact-form"
                              id="contact-form">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Your Name <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-account ml-3 icons"></i>
                                        <input name="name" id="name" type="text" class="form-control pl-5"
                                               placeholder="Your Name :">
                                    </div>
                                </div><!--end col-->

                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label>Your Email <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-email ml-3 icons"></i>
                                        <input name="email" id="email" type="email" class="form-control pl-5"
                                               placeholder="Your email :">
                                    </div>
                                </div><!--end col-->
                                <div class="col-md-6">
                                    <div class="form-group position-relative">
                                        <label>Your Phone <span class="text-danger">*</span></label>
                                        <i class="mdi mdi-phone ml-3 icons"></i>
                                        <input name="phone" id="phone" type="number" class="form-control pl-5"
                                               placeholder="Your Phone Number :">
                                    </div>
                                </div><!--end col-->
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Subject</label>
                                        <i class="mdi mdi-book ml-3 icons"></i>
                                        <input name="subject" id="subject" type="text" class="form-control pl-5"
                                               placeholder="Subject">
                                    </div>
                                </div><!--end col-->
                                <div class="col-md-12">
                                    <div class="form-group position-relative">
                                        <label>Comments</label>
                                        <i class="mdi mdi-comment-text-outline ml-3 icons"></i>
                                        <textarea name="message" id="comments" rows="4" class="form-control pl-5"
                                                  placeholder="Your Message :"></textarea>
                                    </div>
                                </div>
                            </div><!--end row-->
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <input type="submit" id="submit" name="send"
                                           class="submitBnt btn btn-primary btn-block" value="Send Message">
                                    <div id="simple-msg"></div>
                                </div><!--end col-->
                            </div><!--end row-->
                        </form><!--end form-->
                    </div><!--end custom-form-->
                </div>
            </div><!--end col-->

            <div class="col-lg-7 col-md-6 order-1 order-md-2">
                <img src="{{asset('new/get-in-touch.png')}}" class="img-fluid" alt="">
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->

    {{--<div class="container-fluid mt-100 mt-60">
        <div class="row">
            <div class="col-12 p-0">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d39206.002432144705!2d-95.4973981212445!3d29.709510002925988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640c16de81f3ca5%3A0xf43e0b60ae539ac9!2sGerald+D.+Hines+Waterwall+Park!5e0!3m2!1sen!2sin!4v1566305861440!5m2!1sen!2sin" style="border:0" allowfullscreen></iframe>
                </div>
            </div><!--end col-->
        </div><!--end row-->
    </div><!--end container-->--}}
</section><!--end section-->
<!-- End contact -->
