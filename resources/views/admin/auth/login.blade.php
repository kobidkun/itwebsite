

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.shreethemes.in/landrick/layouts/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 29 Feb 2020 02:22:43 GMT -->
<head>
    <meta charset="utf-8" />
    <title>Login to {{config('app.name')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="bootstrap 4, premium, marketing, multipurpose" />
    <meta name="author" content="Shreethemes" />
    <meta name="Version" content="2.0" />
    <!-- favicon -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <!-- Bootstrap -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="{{asset('css/materialdesignicons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Main Css -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="{{asset('css/colors/default.css')}}" rel="stylesheet" id="color-opt">

</head>

<body>
<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
</div>
<!-- Loader -->

<div class="back-to-home rounded d-none d-sm-block">
    <a href="{{route('frontend.index')}}" class="text-white rounded d-inline-block text-center"><i class="mdi mdi-home"></i></a>
</div>

<!-- Hero Start -->
<section class="bg-home d-flex align-items-center">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-6">
                <div class="mr-lg-5">
                    <img src="{{asset('images/user/login.png')}}" class="img-fluid d-block mx-auto" alt="">
                </div>
            </div>
            <div class="col-lg-5 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <div class="login-page bg-white shadow rounded p-4">
                    <div class="text-center">
                        <h4 class="mb-4">Login</h4>
                    </div>
                    <form class="login-form" method="post" action="{{ route('admin.auth.loginAdmin') }}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group position-relative">
                                    <label>Your Email <span class="text-danger">*</span></label>
                                    <i class="mdi mdi-account ml-3 icons"></i>
                                    <input type="email" class="form-control pl-5" placeholder="Email" name="email" required="">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group position-relative">
                                    <label>Password <span class="text-danger">*</span></label>
                                    <i class="mdi mdi-key ml-3 icons"></i>
                                    <input type="password" name="password" class="form-control pl-5" placeholder="Password"  required="">
                                </div>
                            </div>

                            <div class="col-lg-12">
                                {{--                                <p class="float-right forgot-pass"><a href="page-recovery-password.html" class="text-dark font-weight-bold">Forgot password ?</a></p>--}}
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Remember me</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 mb-0">
                                <button class="btn btn-primary w-100">Sign in</button>
                            </div>
                            {{--<div class="col-lg-12 mt-4 text-center">
                                <h6>Or Login With</h6>
                                <ul class="list-unstyled social-icon mb-0 mt-3">
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i class="mdi mdi-facebook" title="Facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i class="mdi mdi-google-plus" title="Google"></i></a></li>
                                    <li class="list-inline-item"><a href="javascript:void(0)" class="rounded"><i class="mdi mdi-github-circle" title="Github"></i></a></li>
                                </ul><!--end icon-->
                            </div>--}}
                            <div class="col-12 text-center">
                                <p class="mb-0 mt-3"><small class="text-dark mr-2">Don't have an account ?</small> <a href="{{route('register')}}" class="text-dark font-weight-bold">Sign Up</a></p>
                            </div>
                        </div>
                    </form>
                </div><!---->
            </div> <!--end col-->
        </div><!--end row-->
    </div> <!--end container-->
</section><!--end section-->
<!-- Hero End -->

<!-- Start Style switcher -->

<!-- End Style switcher -->

<!-- javascript -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.min.js')}}"></script>
<script src="{{asset('js/scrollspy.min.js')}}"></script>
<!-- Switcher -->
<script src="{{asset('js/switcher.js')}}"></script>
<!-- Main Js -->
<script src="{{asset('js/app.js')}}"></script>
</body>

<!-- Mirrored from www.shreethemes.in/landrick/layouts/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 29 Feb 2020 02:22:52 GMT -->
</html>

