@extends('admin.base')
@section('title', 'Admin Dashboard')
@section('content')

    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor"> Customer</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active"> Customer</li>
                </ol>
            </div>
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->



            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Create Payment</h4>
                        </div>
                        <div class="card-body">


                            <form action="{{route('admin.customer.payment.add')}}"  method="post"  >
                                @csrf
                                <div class="form-body">
                                    <h3 class="card-title">Create Payment</h3>
                                    <hr>
                                    <div class="row p-t-20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Amount</label>
                                                <input type="number"  class="form-control" name="amount" placeholder="Amount">
                                            </div>
                                        </div>


                                        <input type="hidden" name="user_id" value="{{$customer->id}}">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Purpose</label>
                                                <input type="text"  class="form-control" name="purpose" placeholder="purpose">
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <label class="control-label">Gateway</label>

                                                <select name="gateway" id="" class="form-control">
                                                    <option value="1">Paypal</option>
                                                    <option value="2">Credit Card / Debit Card / Google Pay / Apple Pay **</option>
                                                    <option value="2">Credit Card / Debit Card (Less Chargeback)</option>
                                                    <option value="3">Account Transfer</option>
                                                    <option value="4">Cheque</option>
                                                    <option value="5">Europe Credit Card / Debit Card</option>

                                                </select>

                                                <p>* </p>


                                            </div>
                                            <!--/span-->
                                        </div>








                                    </div>



                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>

                                    </div>

                                </div>
                            </form>




                        </div>
                    </div>
                </div>
            </div>






            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Create Users</h4>
                        </div>
                        <div class="card-body">


                            <table class="table table-striped table-bordered table-responsive" id="user-table" style="width: 100%">
                                <thead class="thead-dark">
                                <tr>
                                    <th>id</th>
                                    <th>Amt ($)</th>
                                    <th>Gateway</th>
                                    <th>Status</th>
                                    <th>Comment</th>
                                    <th>Direct Link</th>
                                    <th>ACTION</th>



                                </tr>
                                </thead>
                            </table>






                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            @include('admin.component.right-sidebar')
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->

    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->

@stop




@section('footer')

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js">

    </script>




    <script>
        $(function() {
            $('#user-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.payment.all.api',$customer->id)}}',
                buttons: [
                    'copy', 'excel', 'pdf'
                ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'amount', name: 'amount' },
                    { data: 'gateway', name: 'gateway' },

                    { data: 'status', name: 'status' },
                    { data: 'comment', name: 'comment' },
                    { data: 'srturl', name: 'srturl' },

                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>



    @endsection
