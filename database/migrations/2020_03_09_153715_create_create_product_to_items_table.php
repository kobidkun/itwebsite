<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateProductToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_product_to_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('create_product_id');
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->text('pricing')->nullable();
            $table->string('currency')->default('usd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_product_to_items');
    }
}
