<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PageController@index')->name('frontend.index');
Auth::routes();

Route::get('/home', 'HomeController@newindex')->name('home');
Route::get('/dashboard', 'HomeController@newindex')->name('auth.home');

Route::get('/about', 'PageController@about')->name('frontend.about');
Route::get('/contact', 'PageController@contact')->name('frontend.contact');
Route::post('/contact', 'PageController@createContact')->name('frontend.contact.post');
Route::get('/services', 'PageController@services')->name('frontend.services');
Route::get('/payment/success', 'Customer\ManagePayment@PaySuccess')->name('frontend.pay.success');



Route::get('/dashboard', 'Customer\ManagePayment@MakePayment')->name('frontend.dashboard');
Route::get('/process/{uuid}', 'PublicController@GetPayments')->name('frontend.txts.public');
Route::post('/process/payment', 'PublicController@ProcessPayments')->name('frontend.txts.public.process');
Route::post('/payprocess', 'Customer\ManagePayment@PayProcess')->name('frontend.payprocess');
Route::get('/web/pay/{id}', 'Customer\ManageWebPayment@MakePaymentRequest')->name('frontend.web.pay');
Route::post('/web/pay', 'Customer\ManageWebPayment@MakePaymentRequestsend')->name('frontend.web.pay.send');
Route::post('/web/pay/update', 'Customer\ManageWebPayment@updatepaymentstatus')->name('frontend.web.pay.update');

Route::prefix('service')->group(function () {
    Route::get('website_development', 'PageController@serviceWebDev')->name('frontend.service.webdev');
    Route::get('website_maintenance', 'PageController@serviceWebMaintain')->name('frontend.service.webmaintain');
    Route::get('ecommerce', 'PageController@serviceEcommerce')->name('frontend.service.ecommerce');
    Route::get('domain_server', 'PageController@serviceDomainServer')->name('frontend.service.domainserver');
    Route::get('digital_marketing', 'PageController@serviceDigiMart')->name('frontend.service.digimart');
    Route::get('seo', 'PageController@serviceSeo')->name('frontend.service.seo');
});

Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('dashboard', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('register', 'Admin\AdminController@create')->name('admin.register');
    Route::post('register', 'Admin\AdminController@store')->name('admin.register.store');
    Route::get('login', 'Admin\LoginController@login')->name('admin.auth.login');
    Route::post('login', 'Admin\LoginController@loginAdmin')->name('admin.auth.loginAdmin');
    Route::post('logout', 'Admin\LoginController@logout')->name('admin.auth.logout');
    Route::post('customer/add', 'Admin\ManageCustomer@CreateCustomer')->name('admin.customer.add');
    Route::get('customer/all', 'Admin\ManageCustomer@ListCustomer')->name('admin.customer.all');
    Route::get('customer/all/api', 'Admin\ManageCustomer@GetCustonmerAPI')->name('admin.customer.all.api');
    Route::get('customer/{id}', 'Admin\ManageCustomer@CustomerDetails')->name('admin.customer.details');
    Route::post('/customer/payment/add', 'Admin\ManagePayment@CreatePayment')->name('admin.customer.payment.add');
    Route::get('/customer/payment/{id}/all', 'Admin\ManagePayment@AllPaymentApi')->name('admin.customer.payment.all.api');
    Route::get('/customer/payment/details/{uuid}', 'Admin\ManagePayment@GetpaymentDetails')->name('admin.customer.payment.uuid');
    Route::get('/product/create', 'Admin\ManageProduct@CreateProduct')->name('admin.product.crate');
    Route::post('/product/create', 'Admin\ManageProduct@CreateProductSave')->name('admin.product.crate.save');


});
