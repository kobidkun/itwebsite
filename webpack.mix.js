const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.styles([
    'public/css/bootstrap.min.css',
    'public/css/magnific-popup.css',
    'public/css/materialdesignicons.min.css',
    'public/css/owl.carousel.min.css',
    'public/css/owl.theme.default.min.css',
    'public/css/style.css',
    'public/css/colors/default.css',
], 'public/generated/style.min.css');

mix.scripts([
    'public/js/jquery.min.js',
    'public/js/bootstrap.bundle.min.js',
    'public/js/jquery.easing.min.js',
    'public/js/scrollspy.min.js',
    'public/js/jquery.magnific-popup.min.js',
    'public/js/magnific.init.js',
    'public/js/owl.carousel.min.js',
    'public/js/owl.init.js',
    'public/js/app.js',
], 'public/generated/script.min.js');




